module.exports = {
  port: process.env.PORT || 3001,
  db: process.env.MONGODB_URI || 'mongodb://localhost:27017/visto',
  SECRET_TOKEN: process.env.SECRET_TOKEN ||'miclavedetokens',
  NODE_ENV: process.env.NODE_ENV || 'development',
  HOST_ENCLOSURE_DATA: process.env.HOST_ENCLOSURE_DATA || 'localhost'
}
