'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const StorageRackSchema = Schema({
  _id: String,
  cpd: { type: String, lowercase: true },
  items: Object
})

module.exports = mongoose.model('StorageRack', StorageRackSchema)
