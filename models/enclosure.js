'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema

const EnclosureSchema = Schema({
  _id: String,
  data: Object
})

module.exports = mongoose.model('Enclosure', EnclosureSchema)
