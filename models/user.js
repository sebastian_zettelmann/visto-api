'use strict'

const mongoose = require('mongoose')
const Schema = mongoose.Schema
//const bcrypt = require('bcrypt-nodejs')
const crypto = require('crypto')

const UserSchema = new Schema({
  username: { type: String, unique: true, lowercase: true },
  password: { type: String, select: true },
  signupDate: { type: Date, default: Date.now() },
  lastLogin: Date
})


module.exports = mongoose.model('User', UserSchema)
