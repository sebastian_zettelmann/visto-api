'use strict'

const EnclosureRack = require('../models/enclosurerack')

const config = require('../config')


function getEnclosureRack(req, res) {
  let enclosurerackId = req.params.enclosurerackId

  EnclosureRack.findById(enclosurerackId, (err, enclosurerack) => {
    if (err) return res.status(500).send({
      message: `Error al realizar la peticion: ${err}`
    })
    if (!enclosurerack) return res.status(404).send({
      message: `El enclosurerack no existe`
    })

    res.status(200).send({
      enclosurerack
    })
  })
}

function getEnclosureRacks(req, res) {
  let select = new Object();
  if (req.query.select) {
    const keys = req.query.select.split(',');
    keys.forEach(function (k) {
      select[k] = 1
    })
  }

  EnclosureRack.find({}, select, {
    sort: {
      '_id': 'ascending'
    }
  }, (err, enclosureracks) => {
    if (err) return res.status(500).send({
      message: `Error al realizar la peticion: ${err}`
    })
    if (!enclosureracks) return res.status(404).send({
      message: 'No existen enclosureracks'
    })

    res.status(200).send({
      enclosureracks
    })
  })
}

function saveEnclosureRack(req, res) {

  if (!req.body.name)
    return res.status(500).send({
      message: `Error no se recibe el nombre de enclosurerack`
    })
  let enclosurerackId = req.body.name;

  EnclosureRack.findById(enclosurerackId, (err, enclosurerack) => {
    if (err) res.status(500).send({
      message: `Error al buscar el enclosurerack: ${enclosurerackId}`
    })

    if (enclosurerack) return res.status(404).send({
      message: `El enclosurerack ya existe`
    })



    let enclosurerackNew = new EnclosureRack()
    enclosurerackNew._id = req.body.name
    enclosurerackNew.cpd = req.body.cpd
    enclosurerackNew.items = req.body.items

    console.log(enclosurerackNew)

    enclosurerackNew.save((err, enclosurerackStored) => {
      if (err) res.status(500).send({
        message: `Error al salvar en la base de datos: ${err} `
      })

      res.status(200).send({
        enclosurerack: enclosurerackStored
      })
    })



  })




}

function updateEnclosureRack(req, res) {
  let enclosurerackId = req.params.enclosurerackId

  EnclosureRack.findByIdAndUpdate(enclosurerackId, {
    'cpd': req.body.cpd,
    'items': req.body.items
  }, {
    new: true
  }, (err, enclosurerackUpdated) => {
    if (err) res.status(500).send({
      message: `Error al actualizar el enclosurerack: ${err}`
    })

    if (!enclosurerackUpdated) return res.status(404).send({
      message: `El enclosurerack no existe`
    })

    res.status(200).send({
      enclosurerack: enclosurerackUpdated
    })
  })




}

function deleteEnclosureRack(req, res) {
  let enclosurerackId = req.params.enclosurerackId

  EnclosureRack.findById(enclosurerackId, (err, enclosurerack) => {
    if (err) res.status(500).send({
      message: `Error al borrar el enclosurerack: ${err}`
    })

    if (!enclosurerack) return res.status(404).send({
      message: `El enclosurerack no existe`
    })

    enclosurerack.remove(err => {
      if (err) res.status(500).send({
        message: `Error al borrar el enclosurerack: ${err}`
      })
      res.status(200).send({
        message: 'El enclosurerack ha sido eliminado'
      })
    })
  })
}

module.exports = {
  getEnclosureRack,
  getEnclosureRacks,
  saveEnclosureRack,
  updateEnclosureRack,
  deleteEnclosureRack
}