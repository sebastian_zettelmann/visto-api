'use strict'

const StorageRack = require('../models/storagerack')

const config = require('../config')


function getStorageRack(req, res) {
  let storagerackId = req.params.storagerackId

  StorageRack.findById(storagerackId, (err, storagerack) => {
    if (err) return res.status(500).send({
      message: `Error al realizar la peticion: ${err}`
    })
    if (!storagerack) return res.status(404).send({
      message: `El storagerack no existe`
    })

    res.status(200).send({
      storagerack
    })
  })
}

function getStorageRacks(req, res) {
  let select = new Object();
  if (req.query.select) {
    const keys = req.query.select.split(',');
    keys.forEach(function (k) {
      select[k] = 1
    })
  }

  StorageRack.find({}, select, {
    sort: {
      '_id': 'ascending'
    }
  }, (err, storageracks) => {
    if (err) return res.status(500).send({
      message: `Error al realizar la peticion: ${err}`
    })
    if (!storageracks) return res.status(404).send({
      message: 'No existen storageracks'
    })

    res.status(200).send({
      storageracks
    })
  })
}

function saveStorageRack(req, res) {

  if (!req.body.name)
    return res.status(500).send({
      message: `Error no se recibe el nombre de storagerack`
    })
  let storagerackId = req.body.name;

  StorageRack.findById(storagerackId, (err, storagerack) => {
    if (err) res.status(500).send({
      message: `Error al buscar el storagerack: ${storagerackId}`
    })

    if (storagerack) return res.status(404).send({
      message: `El storagerack ya existe`
    })



    let storagerackNew = new StorageRack()
    storagerackNew._id = req.body.name
    storagerackNew.cpd = req.body.cpd
    storagerackNew.items = req.body.items

    console.log(storagerackNew)

    storagerackNew.save((err, storagerackStored) => {
      if (err) res.status(500).send({
        message: `Error al salvar en la base de datos: ${err} `
      })

      res.status(200).send({
        storagerack: storagerackStored
      })
    })



  })




}

function updateStorageRack(req, res) {
  let storagerackId = req.params.storagerackId

  StorageRack.findByIdAndUpdate(storagerackId, {
    'cpd': req.body.cpd,
    'items': req.body.items
  }, {
    new: true
  }, (err, storagerackUpdated) => {
    if (err) res.status(500).send({
      message: `Error al actualizar el storagerack: ${err}`
    })

    if (!storagerackUpdated) return res.status(404).send({
      message: `El storagerack no existe`
    })

    res.status(200).send({
      storagerack: storagerackUpdated
    })
  })




}

function deleteStorageRack(req, res) {
  let storagerackId = req.params.storagerackId

  StorageRack.findById(storagerackId, (err, storagerack) => {
    if (err) res.status(500).send({
      message: `Error al borrar el storagerack: ${err}`
    })

    if (!storagerack) return res.status(404).send({
      message: `El storagerack no existe`
    })

    storagerack.remove(err => {
      if (err) res.status(500).send({
        message: `Error al borrar el storagerack: ${err}`
      })
      res.status(200).send({
        message: 'El storagerack ha sido eliminado'
      })
    })
  })
}

module.exports = {
  getStorageRack,
  getStorageRacks,
  saveStorageRack,
  updateStorageRack,
  deleteStorageRack
}