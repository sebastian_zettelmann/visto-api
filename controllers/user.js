'use strict'

const User = require('../models/user')
const service = require('../services')
const bcrypt = require('bcrypt-nodejs')

function signUp(req, res) {
  const user = new User({
    username: req.body.username,
    //displayName: req.body.displayName,
    password: req.body.password
  })
  console.log('signUp')
  console.log(user)


  User.findOne({
    username: req.body.username
  }, (err, usr) => {
    if (err) return res.status(500).send({
      message: err
    })
    if (usr) {
      console.log(`Ya existe el usuario ${req.body.username}`)
      return res.status(404).send({
        message: `Ya existe el usuario ${req.body.username}`
      })
    } else {
      bcrypt.genSalt(10, (err, salt) => {
        if (err) return next(err)

        bcrypt.hash(user.password, salt, null, (err, hash) => {
          if (err) return next(err)

          user.password = hash
          //next()
        })
      })

      user.save((err) => {
        if (err) return res.status(500).send({
          message: `Error al crear el usuario: ${err}`
        })
        return res.status(201).send({
          token: service.createToken(user)
        })
      })
    }
  })

}

function signIn(req, res) {
  console.log(req.body)
  User.findOne({
    username: req.body.username
  }, (err, user) => {
    if (err) return res.status(500).send({
      message: err
    })
    console.log(user)
    if (!user) return res.status(404).send({
      message: 'No existe el usuario'
    })

    bcrypt.compare(req.body.password, user.password, function (err, result) {
      if (result == true) {
        req.user = user
        res.status(200).send({
          message: 'Te has logueado correctamente',
          token: service.createToken(user)
        })
        
      } else {
        return res.status(404).send({
          message: 'Contraseña incorrecta'
        })
      }
    });

  })
}

function getUsers(req, res) {
  console.log(req.body)
  User.find({}, (err, users) => {
    if (err) return res.status(500).send({
      message: `Error al realizar la petición: ${err}`
    })
    if (!users) return res.status(404).send({
      message: 'No existen usuarios'
    })

    res.send(200, {
      users
    })
  })
}
module.exports = {
  signUp,
  signIn,
  getUsers
}