'use strict'

const Enclosure = require('../models/enclosure')

const config = require('../config')

const http = require('http');
const xml2js = require('xml2js');
const parser = new xml2js.Parser({
  ignoreAttrs: false,
  mergeAttrs: true,
  explicitArray: true
});


function getEnclosure(req, res) {
  let enclosureId = req.params.enclosureId

  Enclosure.findById(enclosureId, (err, enclosure) => {
    if (err) return res.status(500).send({
      message: `Error al realizar la peticion: ${err}`
    })
    if (!enclosure) return res.status(404).send({
      message: `El enclosure no existe`
    })

    res.status(200).send({
      enclosure
    })
  })
}

function getEnclosures(req, res) {
  let select = new Object();
  if (req.query.select) {
    const keys = req.query.select.split(',');
    keys.forEach(function (k) {
      select[k] = 1
    })
  }

  Enclosure.find({}, select, {
    sort: {
      '_id': 'ascending'
    }
  }, (err, enclosures) => {
    if (err) return res.status(500).send({
      message: `Error al realizar la peticion: ${err}`
    })
    if (!enclosures) return res.status(404).send({
      message: 'No existen enclosures'
    })

    res.status(200).send({
      enclosures
    })
  })
}

function saveEnclosure(req, res) {

  if (!req.body.name)
    return res.status(500).send({
      message: `Error no se recibe el nombre de enclosure`
    })
  let enclosureId = req.body.name;

  Enclosure.findById(enclosureId, (err, enclosure) => {
    if (err) res.status(500).send({
      message: `Error al buscar el enclosure: ${enclosureId}`
    })

    if (enclosure) return res.status(404).send({
      message: `El enclosure ya existe`
    })


    let urlXML = `http://${enclosureId}/xmldata?item=all`;
    if (config.NODE_ENV === "development")
      urlXML = `http://${config.HOST_ENCLOSURE_DATA}/${enclosureId}.xml`;

    console.log(urlXML)
    let r = http.get(urlXML, function (response) {
      let data = '';
      
 
      response.on('data', function (stream) {
        data += stream;
      });
      response.on('end', function () {
        parser.parseString(data, function (error, result) {
          if (error === null) {
            console.log("OK")
            let json = JSON.stringify(result);
            let parsed = JSON.parse(json);
            console.log(result)

            let enclosure = new Enclosure()
            enclosure._id = req.body.name
            enclosure.data = parsed
            console.log(enclosure)

            enclosure.save((err, enclosureStored) => {
              if (err) res.status(500).send({
                message: `Error al salvar en la base de datos: ${err} `
              })

              res.status(200).send({
                enclosure: enclosureStored
              })
            })

          } else {
            console.log("error")
            res.status(404).send({
              message: `No se encuentra ${urlXML} `
            })


          }
        });
      });
    }).on('error', function (err) {
      console.log(err);
      res.status(500).send({
        message: `No se pudo acceder a ${urlXML}  `
      })
    });


  })




}

function updateEnclosure(req, res) {
  let enclosureId = req.params.enclosureId


  let urlXML = `http://${enclosureId}/xmldata?item=all`;
  if (config.NODE_ENV === "development")
    urlXML = `http://${config.HOST_ENCLOSURE_DATA}/${enclosureId}.xml`;



  //console.log(urlXML)
  let r = http.get(urlXML, function (response) {
    let data = '';
    

    response.on('data', function (stream) {
      data += stream;
    });
    response.on('end', function () {
      parser.parseString(data, function (error, result) {
        if (error === null) {
          //console.log("OK")
          let json = JSON.stringify(result);
          let update = JSON.parse(json);
          //console.log(result)

          Enclosure.findByIdAndUpdate(enclosureId, {
            'data': update
          }, {
            new: true
          }, (err, enclosureUpdated) => {
            if (err) res.status(500).send({
              message: `Error al actualizar el enclosure: ${err}`
            })

            if (!enclosureUpdated) return res.status(404).send({
              message: `El enclosure no existe`
            })

            res.status(200).send({
              enclosure: enclosureUpdated
            })
          })

        } else {
          console.log("error")
          res.status(404).send({
            message: `No se encuentra ${urlXML} `
          })


        }
      });
    });
  }).on('error', function (err) {
    res.status(500).send({
      message: `No se pudo acceder a ${urlXML}  `
    })
  });


}

function deleteEnclosure(req, res) {
  let enclosureId = req.params.enclosureId

  Enclosure.findById(enclosureId, (err, enclosure) => {
    if (err) res.status(500).send({
      message: `Error al borrar el enclosure: ${err}`
    })

    if (!enclosure) return res.status(404).send({
      message: `El enclosure no existe`
    })

    enclosure.remove(err => {
      if (err) res.status(500).send({
        message: `Error al borrar el enclosure: ${err}`
      })
      res.status(200).send({
        message: 'El enclosure ha sido eliminado'
      })
    })
  })
}

module.exports = {
  getEnclosure,
  getEnclosures,
  saveEnclosure,
  updateEnclosure,
  deleteEnclosure
}