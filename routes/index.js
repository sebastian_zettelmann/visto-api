'use strict'

const express = require('express')
const userCtrl = require('../controllers/user')
const enclosureCtrl = require('../controllers/enclosure')

const enclosurerackCtrl = require('../controllers/enclosurerack')
const storagerackCtrl = require('../controllers/storagerack')

const auth = require('../middlewares/auth')
const api = express.Router()

api.get('/user', userCtrl.getUsers)
api.post('/signup', userCtrl.signUp)
api.post('/signin', userCtrl.signIn)


api.get('/enclosure', enclosureCtrl.getEnclosures)
api.get('/enclosure/:enclosureId', enclosureCtrl.getEnclosure)
api.post('/enclosure', enclosureCtrl.saveEnclosure)
api.put('/enclosure/:enclosureId', enclosureCtrl.updateEnclosure)
api.delete('/enclosure/:enclosureId', enclosureCtrl.deleteEnclosure)
/*
api.post('/enclosure', auth, enclosureCtrl.saveEnclosure)
api.put('/enclosure/:enclosureId', auth, enclosureCtrl.updateEnclosure)
api.delete('/enclosure/:enclosureId', auth, enclosureCtrl.deleteEnclosure)
*/

api.get('/enclosurerack', enclosurerackCtrl.getEnclosureRacks)
api.get('/enclosurerack/:enclosurerackId', enclosurerackCtrl.getEnclosureRack)
api.post('/enclosurerack', enclosurerackCtrl.saveEnclosureRack)
api.put('/enclosurerack/:enclosurerackId', enclosurerackCtrl.updateEnclosureRack)
api.delete('/enclosurerack/:enclosurerackId', enclosurerackCtrl.deleteEnclosureRack)

api.get('/storagerack', storagerackCtrl.getStorageRacks)
api.get('/storagerack/:storagerackId', storagerackCtrl.getStorageRack)
api.post('/storagerack', storagerackCtrl.saveStorageRack)
api.put('/storagerack/:storagerackId', storagerackCtrl.updateStorageRack)
api.delete('/storagerack/:storagerackId', storagerackCtrl.deleteStorageRack)

module.exports = api
